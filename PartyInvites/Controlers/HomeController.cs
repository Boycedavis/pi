﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PartyInvites.Models;
namespace PartyInvites.Controlers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int hour = DateTime.Now.Hour;

            string greeting = hour < 12 ? "Good Morning" : "Good Afternoon";

            ViewBag.Greeting = greeting;

             return View();
            
        }

        [HttpGet]
        public ActionResult RsvpForm()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult RsvpForm(Guest guest)
        {
            if (ModelState.IsValid)
            {
                Repository.AddResponse(guest);
                return View("Thanks", guest);

            }
            else
            {
                //post-back with validation error
                return View();
            }
            
        }
        public ActionResult ListResponses() 
        {
            return View(Repository.Responses.Where(r => r.WillAttend == true));
        }
    }
}
